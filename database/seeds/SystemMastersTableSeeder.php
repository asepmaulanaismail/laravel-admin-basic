<?php

use Illuminate\Database\Seeder;

class SystemMastersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Roles
        DB::table('system_master')->insert([
            'sys_cat' => "LIST",
            'sys_sub_cat' => "ROLE",
            'sys_code' => "1",
            'sys_val' => "Admin",
            'sys_desc' => "Admin's Role",
        ]);
        DB::table('system_master')->insert([
            'sys_cat' => "LIST",
            'sys_sub_cat' => "ROLE",
            'sys_code' => "2",
            'sys_val' => "User",
            'sys_desc' => "User's Role",
        ]);
    }
}
