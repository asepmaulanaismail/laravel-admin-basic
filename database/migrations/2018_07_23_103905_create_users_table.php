<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 25);
            $table->string('password', 100);
            $table->integer('role_id');
            $table->dateTime('last_login')->nullable();
            $table->dateTime('created_dt')->default(date('Y-m-d H:i:s'));
            $table->string('created_by')->default("system");
            $table->dateTime('updated_dt')->default(date('Y-m-d H:i:s'));
            $table->string('updated_by')->default("system");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
