<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSystemMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_master', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sys_cat', 25);
            $table->string('sys_sub_cat', 25);
            $table->string('sys_code', 25);
            $table->string('sys_val', 25);
            $table->string('sys_desc', 100)->nullable();
            $table->dateTime('created_dt')->default(date('Y-m-d H:i:s'));
            $table->string('created_by')->default("system");
            $table->dateTime('updated_dt')->default(date('Y-m-d H:i:s'));
            $table->string('updated_by')->default("system");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_master');
    }
}
