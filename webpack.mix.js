let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/assets/js/app-login.js', 'public/js')
    .js('resources/assets/js/app-dashboard.js', 'public/js')

.js('resources/assets/js/app-master-user.js', 'public/js')
    .js('resources/assets/js/app-master-user-add.js', 'public/js')
    .js('resources/assets/js/app-master-user-edit.js', 'public/js')

.js('resources/assets/js/app-master-system.js', 'public/js')
    .js('resources/assets/js/app-master-system-add.js', 'public/js')
    .js('resources/assets/js/app-master-system-edit.js', 'public/js')

.sass('resources/assets/sass/app.scss', 'public/css');