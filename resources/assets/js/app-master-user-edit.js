require("./app.js")
const app = new Vue({
    el: '#app',
    data() {
        return {
            config: {
                title: "User",
                apiURL: "/api/user",
                pageURL: "/user",
                fields: [{
                    fieldName: "username",
                    title: "Username",
                    placeholder: "Username",
                    type: "text",
                    required: true,
                    disabled: true,
                    autofocus: false,
                    maxlength: 25
                }, {
                    fieldName: "role_id",
                    title: "Role",
                    placeholder: "Role ID",
                    type: "number",
                    required: true,
                    autofocus: true
                }]
            }
        };
    },
});