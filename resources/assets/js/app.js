/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('admin-lte');
import swal from 'sweetalert'

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('login-form', require('./components/LoginForm.vue'));
Vue.component('main-navigation', require('./components/MainNavigation.vue'));
Vue.component('com-table-basic', require('./components/common/TableBasic.vue'));
Vue.component('com-combo-basic', require('./components/common/ComboboxBasic.vue'));
Vue.component('com-form-basic', require('./components/common/FormBasic.vue'));