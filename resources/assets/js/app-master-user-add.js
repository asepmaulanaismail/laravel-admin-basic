require("./app.js")
const app = new Vue({
    el: '#app',
    data() {
        return {
            config: {
                title: "User",
                apiURL: "/api/user",
                pageURL: "/user",
                fields: [{
                    fieldName: "username",
                    title: "Username",
                    placeholder: "Username",
                    type: "text",
                    required: true,
                    disabled: false,
                    autofocus: true,
                    maxlength: 25
                }, {
                    fieldName: "password",
                    title: "Password",
                    placeholder: "Password",
                    type: "password",
                    required: true,
                    disabled: false,
                    autofocus: false
                }, {
                    fieldName: "confirm_password",
                    title: "Confirm Password",
                    placeholder: "Retype Your Password",
                    type: "password",
                    required: true,
                    disabled: false,
                    autofocus: false
                        // }, {
                        //     fieldName: "role_id",
                        //     title: "Role",
                        //     placeholder: "Role ID",
                        //     type: "combo",
                        //     required: true,
                        //     apiCombo: "/api/user/combo"
                }, {
                    fieldName: "role_id",
                    title: "Role",
                    placeholder: "Role ID",
                    type: "number",
                    required: true
                }]
            }
        };
    },
});