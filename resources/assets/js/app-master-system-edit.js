require("./app.js")
const app = new Vue({
    el: '#app',
    data() {
        return {
            config: {
                title: "System",
                apiURL: "/api/system",
                pageURL: "/system",
                fields: [{
                    fieldName: "sys_cat",
                    title: "Category",
                    placeholder: "Category",
                    type: "text",
                    required: true,
                    disabled: true,
                    autofocus: false,
                    maxlength: 25
                }, {
                    fieldName: "sys_sub_cat",
                    title: "Sub Category",
                    placeholder: "Sub Category",
                    type: "text",
                    required: true,
                    disabled: true,
                    autofocus: false
                }, {
                    fieldName: "sys_code",
                    title: "Code",
                    placeholder: "Code",
                    type: "text",
                    required: true,
                    disabled: true,
                    autofocus: false
                }, {
                    fieldName: "sys_val",
                    title: "Value",
                    placeholder: "Value",
                    type: "text",
                    required: true,
                    disabled: false,
                    autofocus: true
                }, {
                    fieldName: "sys_desc",
                    title: "Description",
                    placeholder: "Description",
                    type: "textarea",
                    required: true,
                    disabled: false,
                    autofocus: false
                }]
            }
        };
    },
});