@extends("layouts/app")
@section("title")
System Master Screen
@endsection
@section("subtitle")
{{ env("APP_NAME", "Laravel") }}
@endsection
@section("breadcrumbs")
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Master</a></li>
    <li><a href="/system">System</a></li>
    <li class="active">Add</li>
</ol>
@endsection

@section("content")
    <com-form-basic :config="config"></com-form-basic>
@endsection
@section("vuescript")
<script src="{{mix('js/app-master-system-add.js')}}"></script>
@endsection