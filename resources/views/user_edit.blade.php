@extends("layouts/app")
@section("title")
User Master Screen
@endsection
@section("subtitle")
{{ env("APP_NAME", "Laravel") }}
@endsection
@section("breadcrumbs")
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Master</a></li>
    <li><a href="/user">Users</a></li>
    <li class="active">Edit</li>
</ol>
@endsection

@section("content")
    <com-form-basic :id="'{{$id}}'" :config="config"></com-form-basic>
@endsection
@section("vuescript")
<script src="{{mix('js/app-master-user-edit.js')}}"></script>
@endsection