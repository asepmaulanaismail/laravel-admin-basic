@extends("layouts/app")
@section("title")
System Master Screen
@endsection
@section("subtitle")
{{ env("APP_NAME", "Laravel") }}
@endsection
@section("breadcrumbs")
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Master</a></li>
    <li class="active">Systems</li>
</ol>
@endsection

@section("content")
    <master-system-view-component><master-system-view-component>
@endsection
@section("vuescript")
<script src="{{mix('js/app-master-system.js')}}"></script>
@endsection