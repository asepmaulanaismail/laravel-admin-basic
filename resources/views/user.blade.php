@extends("layouts/app")
@section("title")
User Master Screen
@endsection
@section("subtitle")
Laravel Admin Basic
@endsection
@section("breadcrumbs")
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Master</a></li>
    <li class="active">Users</li>
</ol>
@endsection

@section("content")
    <master-user-view-component><master-user-view-component>
@endsection
@section("vuescript")
<script src="{{mix('js/app-master-user.js')}}"></script>
@endsection