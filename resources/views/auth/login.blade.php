
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="/favicon.ico">

        <title>Login - {{env('APP_NAME', 'Laravel')}}</title>

        <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
    </head>

    <body class="text-center">
        <div id="app" class="container">
            <login-form></login-form>
        </div>
    </body>
    
    <script src="{{ mix('js/app-login.js') }}"></script>
</html>
