<?php
namespace App\Traits;

use Illuminate\Support\Facades\Session;

trait SessionWrapper
{
    public function remember($key, $value){
        Session::put($key, $value);
    }
  
    public function forget($key){
        Session::forget($key);
    }
  
    public function hasSession($key){
        return Session::has($key);
    }
  
    public function getSession($key){
        return Session::get($key);
    }
  
    public function getAllSession(){
        return Session::all();
    }
}
