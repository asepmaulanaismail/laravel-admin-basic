<?php
namespace App\Traits;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Psr\Http\Message\ResponseInterface;


trait GuzzleWrapper
{
    public $client;

    public function initGuzzleAPI(){
        $this->client =  new Client(['base_uri' => env('APP_BASE_API_URL','https://private-a524f-basic9.apiary-mock.com'),
            'headers' => [
            'Content-Type'=>'application/json'
            ]
        ]);
    }

    public function initGuzzleForm(){
        $this->client =  new Client(['base_uri' => env('APP_BASE_API_URL','https://private-a524f-basic9.apiary-mock.com'),
            'headers' => [
                'Content-Type'=>'application/x-www-form-urlencoded'
                ]
        ]);
    }

    public function get($url){
        try{
            $response = $this->client->get($url);
        if($response->getStatusCode() == 200){
            return json_decode((string) $response->getBody(), true);
        }else{
            return ['status'=>false, 'message'=>config('messages.global_warning')];
        }
        }catch(GuzzleException $e){
            return ['status'=>false, 'message'=>config('messages.global_warning')];
        }
    }

    public function post($url, $body){
        try{
            $response = $this->client->post($url, [
                "json" => $body
            ]);
            if($response->getStatusCode() == 200){
                return json_decode((string) $response->getBody(), true);
            }else{
                return ['status'=>false, 'message'=>$response->getStatusCode()];
            }
        }catch(GuzzleException $e){
            return ['status'=>false, 'message'=>$e];
        }
    }

    public function put($url, $body){
        try{
            $response = $this->client->put($url, [
                "json" => $body
            ]);
            if($response->getStatusCode() == 200){
                return json_decode((string) $response->getBody(), true);
            }else{
                return ['status'=>false, 'message'=>$response->getStatusCode()];
            }
        }catch(GuzzleException $e){
            return ['status'=>false, 'message'=>$e];
        }
    }

    public function delete($url, $body){
        try{
            $response = $this->client->delete($url, [
                "json" => $body
            ]);
            if($response->getStatusCode() == 200){
                return json_decode((string) $response->getBody(), true);
            }else{
                return ['status'=>false, 'message'=>$response->getStatusCode()];
            }
        }catch(GuzzleException $e){
            return ['status'=>false, 'message'=>$e];
        }
    }
}
