<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class System extends Authenticatable
{
    protected $table = "system_master";
    // protected $primaryKey = 'usr_id';
    // public $incrementing = false;
    protected $fillable = [
        'sys_cat', 
        'sys_sub_cat', 
        'sys_code',
        'sys_val',
        'sys_desc'
    ];
    public $timestamps = false;
    protected $hidden = [
        'created_dt', 'created_by', 'updated_dt', 'updated_by',
    ];
}
