<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    // protected $table = "user";
    // protected $primaryKey = 'usr_id';
    // public $incrementing = false;
    protected $fillable = [
        'username', 
        'password', 
        'role_id',
        'last_login'
    ];
    public $timestamps = false;
    protected $hidden = [
        'password', 'created_dt', 'created_by', 'updated_dt', 'updated_by',
    ];
}
