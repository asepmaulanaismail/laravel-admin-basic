<?php

namespace App\Http\Middleware;

use Closure;
use App\Traits\SessionWrapper;

class RedirectIfAuthenticated
{
    use SessionWrapper;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->hasSession("user")) {
            return redirect('/');
        }

        return $next($request);
    }
}
