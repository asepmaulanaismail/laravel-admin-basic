<?php

namespace App\Http\Middleware;

use Closure;
use App\Traits\SessionWrapper;

class RedirectIfNotAuthenticated
{
    use SessionWrapper;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */

    private $adminOnlyPath = [
        'user',
        'system'
    ];
    public function handle($request, Closure $next)
    {
        $data = $this->getAllSession();
        if (!$this->hasSession("user")) {
            if($request->is('api/user/auth')){
                return $next($request);
            }
            if (!$request->ajax()){
                $path = $request->path();
                if ($path != "/")
                    $path = "/".$path;
                return redirect('/login?url='.$path);
            }
            else
                return response()->json(['error' => 'Unauthenticated.'], 401);
        }else{
            // checking role
            $user = $this->getSession("user");
            $path = $request->path();
            if ($user->role_id != 1){
                $allowed = true;
                foreach($this->adminOnlyPath as $key => $value){
                    // echo $path."-".$value.": ".strpos($path, $value)."<br/>";
                    if (strpos($path, $value) !== false){
                        $allowed = false;
                        break;
                    }
                }
                // dd($allowed);
                if (!$allowed){
                    return redirect('/');
                }
            }
        }

        return $next($request);
    }
}
