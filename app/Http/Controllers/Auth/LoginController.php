<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->initGuzzleAPI();
        $this->middleware('guest')->except('logout');
    }

    public function index(){
        return view('auth/login');
    }

    public function auth(Request $request){ 
        $data = json_decode("{}");
        try {    
            $response = $this->post("/api/user/auth", $request->only(["username", "password"]));
            if ($response["status"] != false){
                $data->code = encrypt($response["data"]);
            }else{
                $data->code = "";
                $data->message = $response["message"];
            }
        }
        catch (\Exception $e) {
            $data->code = "";
            $data->message = $e->getMessage();
        }
        return response()->json($data);
    }

    public function authenticated(Request $request){
        $user = $request->input("u");
        $url = "";
        if ($user != ""){
            try {
                $decrypted = decrypt($user);
                $this->remember("user", (object)$decrypted);
                if ($request->has("url")){
                    $url = $request->input("url");
                    if ($url != "" && $url != null && $url != "null")
                        return redirect($url);
                }
                return redirect("/");
            } catch (DecryptException $e) {
                return redirect("/login");
            }
        }else{
            return redirect("/login");
        }
    }

    public function logout(Request $request){
        $this->forget('user');
        return redirect("/");
    }
}
