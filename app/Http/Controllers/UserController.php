<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->initGuzzleAPI();
        $this->middleware('user');
    }

    public function index(Request $request)
    {
        return view("user");
    }
    
    public function add(Request $request)
    {
        return view("user_add");
    }
    
    public function edit(Request $request, $userId)
    {
        return view("user_edit", array("id" => $userId));
    }
}
