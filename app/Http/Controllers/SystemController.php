<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SystemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->initGuzzleAPI();
        $this->middleware('user');
    }

    public function index(Request $request)
    {
        return view("system");
    }
    
    public function add(Request $request)
    {
        return view("system_add");
    }
    
    public function edit(Request $request, $id)
    {
        return view("system_edit", array("id" => $id));
    }
}
