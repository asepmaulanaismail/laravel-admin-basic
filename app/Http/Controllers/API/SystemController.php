<?php

namespace App\Http\Controllers\API;

use App\Models\System;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class SystemController extends Controller
{
    public function __construct()
    {
        $this->resp = json_decode("{}");
        $this->user = $this->getSession("user");
        if ($this->user == null){
            $this->user = json_decode("{}");
            $this->user->id = "postman";
        }
        if (env("APP_ENV", "local") == "production"){
            $this->middleware('user');
        }
    }

    public function index(Request $request){
        try {
            $db = System::select("*")
            ->orderBy("sys_cat");

            if ($request->has("sys_cat") && $request->input("sys_cat") != "")
                $db->where("sys_cat", "like", "%".$request->input("sys_cat")."%");
    
            if ($request->has("sys_sub_cat") && $request->input("sys_sub_cat") != "")
                $db->where("sys_sub_cat", "like", "%".$request->input("sys_sub_cat")."%");
    
            if ($request->has("sys_code") && $request->input("sys_code") != "")
                $db->where("sys_code", "like", "%".$request->input("sys_code")."%");

            $this->resp = $db->paginate(10);
        }
        catch (\Exception $e) {
            $this->resp->data = [];
            $this->resp->status = false;
            $this->resp->message = end($e->errorInfo);
        }
        return response()->json($this->resp);
    }

    public function combo(Request $request){
        try{
            $db = System::select("sys_code as value", "sys_val as text");

            if ($request->has("sys_cat") && $request->input("sys_cat") != "")
                $db->where("sys_cat", "like", "%".$request->input("sys_cat")."%");
    
            if ($request->has("sys_sub_cat") && $request->input("sys_sub_cat") != "")
                $db->where("sys_sub_cat", "like", "%".$request->input("sys_sub_cat")."%");
    
            if ($request->has("sys_code") && $request->input("sys_code") != "")
                $db->where("sys_code", "like", "%".$request->input("sys_code")."%");
                
            $this->resp->items = $db->orderBy("sys_cat")->get();
        }
        catch (\Exception $e) {
            $this->resp->items = [];
            $this->resp->status = false;
            $this->resp->message = end($e->errorInfo);
        }
        return response()->json($this->resp);
    }

    public function show($id){
        $data = System::find($id);
        if ($data != null){
            return response()->json($data);
        }else{
            $this->resp = json_decode("{}");
            $this->resp->status = false;
            $this->resp->message = "System with ID '".$id."' is not found";
            return response()->json($this->resp);
        }
    }

    public function store(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'sys_cat' => 'required|string|max:25',
                'sys_sub_cat' => 'required|string|max:25',
                'sys_code' => 'required|string|max:25',
                'sys_val' => 'required|string|max:25',
                'sys_desc' => 'string|max:100'
            ]);

            if ($validator->fails()){
                $errors = $validator->errors();
                $this->resp->status = false;
                $this->resp->message = $errors->all();
                return response()->json($this->resp);
            }

            $db = new System();
            $db->fill($request->all());
            $db->created_dt = date('Y-m-d H:i:s');
            $db->created_by = $this->user->id;
            $db->updated_dt = date('Y-m-d H:i:s');
            $db->updated_by = $this->user->id;
            $db->save();

            $this->resp->status = true;
            $this->resp->data = $db;
        }
        catch (\Exception $e) {
            $this->resp->items = [];
            $this->resp->status = false;
            $this->resp->message = $e->getMessage();
        }
        return response()->json($this->resp);
    }
    public function update(Request $request, $id){
        try{
            $validator = Validator::make($request->all(), [
                'sys_code' => 'required|string|max:25',
                'sys_val' => 'required|string|max:25',
                'sys_desc' => 'string|max:100'
            ]);

            if ($validator->fails()){
                $errors = $validator->errors();
                $this->resp->status = false;
                $this->resp->message = $errors->all();
                return response()->json($this->resp);
            }

            $db = System::find($id);
            if ($db != null){
                $db->update($request->all());
                $db->updated_dt = date('Y-m-d H:i:s');
                $db->updated_by = $this->user->id;
                $db->save();

                $this->resp->status = true;
                $this->resp->data = $db;
            }else{
                $this->resp->status = false;
                $this->resp->message = ["System with ID '".$id."' is not found"];
            }
        }
        catch (\Exception $e) {
            $this->resp->status = false;
            $this->resp->message = end($e->errorInfo);
        }
        return response()->json($this->resp);
    }
    
    public function deleteData(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'ids' => 'required|string'
            ]);
            $ids = $request->input("ids");
            $error = 0;
            $success = 0;
            foreach($ids as $key => $val){
                $db = System::find($val);
                if ($db != null){
                    $db->delete();
                    $success++;
                }else{
                    $error++;
                }
            }
            $this->resp->status = $success || $error;
            if ($error == 0 && $success > 0){
                    $this->resp->message = ["All data has been deleted."];
            }else{
                if ($error > 0 && $success == 0)
                    $this->resp->message = ["All data can't be deleted."];
                else
                    $this->resp->message = ["Some data can't be deleted."];
            }
        }
        catch (\Exception $e) {
            $this->resp->status = false;
            $this->resp->message = end($e->errorInfo);
        }

        return response()->json($this->resp);
    }
}
