<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->resp = json_decode("{}");
        $this->user = $this->getSession("user");
        if ($this->user == null){
            $this->user = json_decode("{}");
            $this->user->id = "postman";
        }
        if (env("APP_ENV", "local") == "production"){
            $this->middleware('user');
        }
    }
    
    public function auth(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'username' => 'required|string|max:25',
                'password' => 'required|string|min:8'
            ]);

            if ($validator->fails()){
                $errors = $validator->errors();
                $this->resp->status = false;
                $this->resp->message = $errors->all();
                return response()->json($this->resp);
            }

            $username = $request->input("username");
            $password = sha1($request->input("password"));

            $db = User::where("username", $username)->where("password", $password)->get();
            if(count($db) == 0){
                $this->resp->status = false;
                $this->resp->message = "Username/Password is not match.";
            }else{
                User::where('username', $username)
                ->where('password', $password)
                ->update(['last_login' => date('Y-m-d H:i:s')]);
                $this->resp->status = true;
                $this->resp->data = $db[0];
            }
        }
        catch (\Exception $e) {
            $this->resp->status = false;
            $this->resp->message = end($e->errorInfo);
        }
        return response()->json($this->resp);
    }

    public function index(Request $request){
        try {
            $db = User::select("*")
            ->orderBy("username");

            if ($request->has("username"))
                $db->where("username", "like", "%".$request->input("username")."%");

            $this->resp = $db->paginate(10);
        }
        catch (\Exception $e) {
            $this->resp->data = [];
            $this->resp->status = false;
            $this->resp->message = end($e->errorInfo);
        }
        return response()->json($this->resp);
    }

    public function combo(Request $request){
        try{
            $db = User::select("id as value", "username as text");

            if ($request->has("q"))
                $db->where("username", "like", "%".$request->input("q")."%");
                
            $this->resp->items = $db->orderBy("username")->get();
        }
        catch (\Exception $e) {
            $this->resp->items = [];
            $this->resp->status = false;
            $this->resp->message = end($e->errorInfo);
        }
        return response()->json($this->resp);
    }

    public function show($id){
        $data = User::find($id);
        if ($data != null){
            return response()->json($data);
        }else{
            $this->resp = json_decode("{}");
            $this->resp->status = false;
            $this->resp->message = "User with ID '".$id."' is not found";
            return response()->json($this->resp);
        }
    }

    public function store(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'username' => 'required|string|max:25|unique:users',
                'password' => 'required|string|min:8',
                'confirm_password' => 'required|string|min:8',
                'role_id' => 'required|integer'
            ]);

            if ($validator->fails()){
                $errors = $validator->errors();
                $this->resp->status = false;
                $this->resp->message = $errors->all();
                return response()->json($this->resp);
            }

            if ($request->input("password") != $request->input("confirm_password")){
                $this->resp->status = false;
                $this->resp->message = ["Password & Confirm Password didn't match."];
                return response()->json($this->resp);
            }

            $db = new User();
            $db->fill($request->all());
            $db->created_dt = date('Y-m-d H:i:s');
            $db->created_by = $this->user->id;
            $db->updated_dt = date('Y-m-d H:i:s');
            $db->updated_by = $this->user->id;
            $db->save();

            $this->resp->status = true;
            $this->resp->data = $db;
        }
        catch (\Exception $e) {
            $this->resp->items = [];
            $this->resp->status = false;
            $this->resp->message = $e->getMessage();
        }
        return response()->json($this->resp);
    }
    public function update(Request $request, $id){
        try{
            $validator = Validator::make($request->all(), [
                'role_id' => 'required|integer'
            ]);

            if ($validator->fails()){
                $errors = $validator->errors();
                $this->resp->status = false;
                $this->resp->message = $errors->all();
                return response()->json($this->resp);
            }

            $db = User::find($id);
            if ($db != null){
                $db->update($request->all());
                $db->updated_dt = date('Y-m-d H:i:s');
                $db->updated_by = $this->user->id;
                $db->save();

                $this->resp->status = true;
                $this->resp->data = $db;
            }else{
                $this->resp->status = false;
                $this->resp->message = ["User with ID '".$id."' is not found"];
            }
        }
        catch (\Exception $e) {
            $this->resp->status = false;
            $this->resp->message = end($e->errorInfo);
        }
        return response()->json($this->resp);
    }
    public function updatePassword(Request $request, $id){
        try{
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|min:8',
                'confirm_password' => 'required|string|min:8',
            ]);

            if ($validator->fails()){
                $errors = $validator->errors();
                $this->resp->status = false;
                $this->resp->message = $errors->all();
                return response()->json($this->resp);
            }

            if ($request->input("password") != $request->input("confirm_password")){
                $this->resp->status = false;
                $this->resp->message = ["Password & Confirm Password didn't match."];
                return response()->json($this->resp);
            }

            $db = User::find($id);
            if ($db != null){
                $db->password = sha1($request->input("password"));
                $db->updated_dt = date('Y-m-d H:i:s');
                $db->updated_by = $this->user->id;
                $db->save();

                $this->resp->status = true;
                $this->resp->data = $db;
            }else{
                $this->resp->status = false;
                $this->resp->message = ["User with ID '".$id."' is not found"];
            }
        }
        catch (\Exception $e) {
            $this->resp->status = false;
            $this->resp->message = end($e->errorInfo);
        }
        return response()->json($this->resp);
    }
    public function deleteData(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'ids' => 'required|string'
            ]);
            $ids = $request->input("ids");
            $error = 0;
            $success = 0;
            foreach($ids as $key => $val){
                $db = User::find($val);
                if ($db != null){
                    $db->delete();
                    $success++;
                }else{
                    $error++;
                }
            }
            $this->resp->status = $success || $error;
            if ($error == 0 && $success > 0){
                    $this->resp->message = ["All data has been deleted."];
            }else{
                if ($error > 0 && $success == 0)
                    $this->resp->message = ["All data can't be deleted."];
                else
                    $this->resp->message = ["Some data can't be deleted."];
            }
        }
        catch (\Exception $e) {
            $this->resp->status = false;
            $this->resp->message = end($e->errorInfo);
        }

        return response()->json($this->resp);
    }
}
