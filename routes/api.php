<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/auth', 'Auth\LoginController@auth')->name('login_auth');

Route::get('/user', 'API\UserController@index');
Route::post('/user/auth', 'API\UserController@auth');
Route::post('/user', 'API\UserController@store');
Route::get('/user/combo', 'API\UserController@combo');
Route::get('/user/{id}', 'API\UserController@show');
Route::put('/user/{id}', 'API\UserController@update');
Route::put('/user/change-password/{id}', 'API\UserController@updatePassword');
Route::delete('/user', 'API\UserController@deleteData');

Route::get('/system', 'API\SystemController@index');
Route::post('/system', 'API\SystemController@store');
Route::get('/system/combo', 'API\SystemController@combo');
Route::get('/system/{id}', 'API\SystemController@show');
Route::put('/system/{id}', 'API\SystemController@update');
Route::delete('/system', 'API\SystemController@deleteData');