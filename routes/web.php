<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home_index');
Route::get('/login', 'Auth\LoginController@index')->name('login_index');
Route::get('/logout', 'Auth\LoginController@logout')->name('login_logout');
Route::get('/authenticated', 'Auth\LoginController@authenticated')->name('login_authenticated');

Route::get('/user', 'UserController@index')->name('user_index');
Route::get('/user/add', 'UserController@add')->name('user_add');
Route::get('/user/edit/{id}', 'UserController@edit')->name('user_edit');

Route::get('/system', 'SystemController@index')->name('system_index');
Route::get('/system/add', 'SystemController@add')->name('system_add');
Route::get('/system/edit/{id}', 'SystemController@edit')->name('system_edit');
