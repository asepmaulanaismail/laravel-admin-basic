# Laravel Admin Basic

This is BSP's Framework for admin application using Laravel Framework.

## Requirements
1. PHP 7.2.3 (or just install XAMPP with php 7.2.3)
2. Composer
3. Node JS

## How to Run
```
 git pull git@gitlab.com:beesolutionpartners/company-profile.git
 cd company-profile
 cp .env.example .env
 npm install
 npm run dev
 composer install --profile
 php artisan key:generate
 php artisan migrate:fresh --seed
 php artisan serve
```

## Default User Credentials
Admin:
- username : admin
- password : bsp123!!
  
Not Admin:
- username : notadmin
- password : bsp123!!


Happy coding!